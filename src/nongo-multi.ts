import Nongo from './nongo';
import NongoParams from './nongo-params';

/**
 * Used to efficiently set up and maintain multiple nongo connections that use the same models
 */
export default class NongoMulti {

  private nongoMap: any = {};

  constructor(private paramsArray: NongoParams[], private modelClasses: any[]) {
  }

  public async connect(): Promise<NongoMulti> {
    // Make the first nongo connection
    const firstParams = this.paramsArray[0];
    this.checkHasId(firstParams);
    const firstNongo = await new Nongo(firstParams, this.modelClasses).connect();
    this.nongoMap[firstParams.id] = firstNongo;

    // Connect all the other instances using the schemaParsers from  {@code firstNongo}
    for (let i = 1; i < this.paramsArray.length; i++) {
      const params = this.paramsArray[i];
      this.nongoMap[params.id] = await new Nongo(params, this.modelClasses, firstNongo.schemaParsers).connect();
    }

    return this;
  }

  public getNongo(id: string) {
    return this.nongoMap[id];
  }

  private checkHasId(params: NongoParams): void {
    if (params.id == null) {
      throw Error('One the NongoParams objects given is missing an id field');
    }
  }

}
