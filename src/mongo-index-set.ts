import MongoIndex from './mongo-index';

/**
 * Collection used to maintain a set of {@link MongoIndex}s. {@link MongoIndex}s are concidered the same if they have
 * a matching {@code name}.
 */
export default class MongoIndexSet {

  private indexMap = new Map<string, MongoIndex>();

  public add(mIndex: MongoIndex) {
    this.indexMap.set(mIndex.name, mIndex);
  }

  public get(mIndex: MongoIndex): MongoIndex {
    return this.indexMap.get(mIndex.name);
  }

  public has(mIndex: MongoIndex) {
    this.indexMap.has(mIndex.name);
  }

  public values(): IterableIterator<MongoIndex> {
    return this.indexMap.values();
  }

  public remove(mIndex): boolean {
    return this.indexMap.delete(mIndex.name);
  }

}
