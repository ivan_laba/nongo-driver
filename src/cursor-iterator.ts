import {Cursor} from 'mongodb';

export default class CursorIterator<T = any> implements AsyncIterator<T> {

  constructor(private cursor: Cursor, private map = (next) => next) {
  }

  public [Symbol.asyncIterator] = () => this;

  public async next(): Promise<IteratorResult<T>> {
    const next = await this.cursor.next();
    return {
      value: this.map(next),
      done: next == null,
    };
  }

}
