import clone from 'clone-deep';
import cmd from 'cmd-promise';
import {Db, ObjectId} from 'mongodb';

import CursorIterator from './cursor-iterator';
import {NongoDeleteResult, NongoWriteResult} from './interfaces';
import MongoIndex from './mongo-index';
import Nongo from './nongo';
import Validator from './validator';

import Logger from 'wbb-logger';
const logger = new Logger('schema-parser.ts');

export default abstract class Model<T extends { _id?: ObjectId } = any> {
  // {@code instanceof} can be a bit funny when importing classes across
  // modules so this is an explicit type guard we can use
  public isNongoModel: true = true;
  public collection: string;
  public db: Db;
  public name: string;
  public dynamic: boolean;

  constructor(public nongo: Nongo, public obj: T) {
    // Set this.db if nongo given
    if (nongo) {
      this.db = nongo.db;
    }

    // If the _id is a string turn it into an {@code ObjectId}
    if (this.obj && typeof this.obj._id === 'string') {
      this.obj._id = new ObjectId(this.obj._id);
    }

    this.collection = this.defineCollection();
    this.name = this.constructor.name;
    this.dynamic = false;
    this.initStructure();
  }

  public getMongoIndexes(): MongoIndex[] {
    return [];
  }

  public async validate() {
    const schema = this.nongo.schema[this.name];
    return await new Validator(schema, this).validate();
  }

  public async dumpCollection(dir: string, gzip = false): Promise<void> {
    logger.info(`Taking dump of ${this.name}...`);

    const script =
      `mkdir -p ${dir};` +
      `mongodump --uri "${this.nongo.uri()}" -c ${this.name} ${gzip ? '--gzip' : ''} --out ${dir};`;

    await cmd(script);
    logger.info(`Dump written to ${dir}...`);
  }

  public idString(): string {
    return this.obj._id.toHexString();
  }

  /*
   * Upsert the model into the database
   */
  public async save() {
    // Set the object id if there isn't one already
    this.obj._id = Nongo.toObjectId(this.obj._id);

    // Make a deep clone so that the original object does not have it's fields striped
    this.obj = clone(this.obj);
    // Strip all the keys from the object that are not present in the schema
    // if not a dynamic model
    if (!this.dynamic) {
      this.strip('', this.obj);
    }
    // Validate the object
    const err = await this.validate();
    if (err) {
      this.throwValidationError(err);
    }

    // Upsert
    await this.db
      .collection(this.collection)
      .replaceOne({_id: this.obj._id}, this.obj, {upsert: true});

    // Return the saved model
    return this;
  }

  /*
   * Recursive method used to strip fields from an object have not been defined
   * by the schema.
   */
  public strip(parentPath, toStrip) {
    for (const key of Object.keys(toStrip)) {
      const path = parentPath ? parentPath + '.' + key : key;

      // Skip over keys which shouldn't be stripped
      if (this.nongo.noStripKeys[this.constructor.name].has(path)) {
        continue;
      }

      // If the key is not in the schema delete it
      if (!this.nongo.validKeys[this.constructor.name].has(path)) {
        this.deleteKey(path, this.obj);

        // If the key is in the schema examin it's children if there are any
      } else {
        const value = toStrip[key];

        // If value is an array call strip on all of it's elements that are objects
        if (Array.isArray(value)) {
          value.forEach((element) => {
            if (typeof element === 'object') {
              this.strip(path, element);
            }
          });

          // If the value is a nested object call strip on it
        } else if (value != null && typeof value === 'object') {
          this.strip(path, value);
        }
      }
    }
  }

  /*
   * Recursive method used to delete keys from models using a schema path.
   * This includeds the handling of arrays
   *
   * @param the path to the key which should be deleted.
   * @param the object to which the path is relative.
   */
  public deleteKey(path, obj) {
    if (!obj) {
      return;
    }

    // If there is no . remaining in the path then delete the required key
    const indexFirstDot = path.indexOf('.');
    if (indexFirstDot === -1) {
      delete obj[path];
    } else {
      // Make a recusive call dependant on the value type
      const value = obj[path.slice(0, indexFirstDot)];
      const pathRemaining = path.slice(indexFirstDot + 1);
      if (Array.isArray(value)) {
        value.forEach((el) => this.deleteKey(pathRemaining, el));
      } else {
        this.deleteKey(pathRemaining, value);
      }
    }
  }

  public async byId(id) {
    return await this.findOne({_id: id});
  }

  /*
   * @param query: A mongo db query.
   * @param options: the query options e.g. sorting, limiting results etc.
   * @return the results of the query as an array of models.
   */
  public async find(query: any = {}, options?: any): Promise<this[]> {
    return this.toArray(await this.findIterator(query, options));
  }

  /*
   * @param query: A mongo db query.
   * @param options: the query options e.g. sorting, limiting results etc.
   * @return the results of the query as an {@link AsyncIterator}.
   */
  public async findIterator(
    query: any = {},
    options?: any,
  ): Promise<CursorIterator<this>> {
    query = this.prepareQuery(query);
    const cursor = this.db.collection(this.collection).find(query, options);
    return new CursorIterator<this>(
      cursor,
    // @ts-ignore
      (obj) => new this.constructor(this.nongo, obj),
    );
  }

  /*
   * @param query: A mongo db query.
   * @return the result of the query as a model.
   */
  public async findOne(query: any = {}): Promise<this> {
    query = this.prepareQuery(query);
    const doc = await this.db.collection(this.collection).findOne(query);

    // Return null if no doc was found
    if (doc == null) {
      return null;
    }

    // Otherwise return the doc as a model
    // @ts-ignore
    return new this.constructor(this.nongo, doc);
  }

  public async remove(query: any): Promise<NongoDeleteResult> {
    query = this.prepareQuery(query);
    const res = await this.db.collection(this.collection).deleteMany(query);
    return res.result;
  }

  public async aggregate(pipeline: any, options?: any): Promise<any[]> {
    return await this.toArray(await this.aggregateIterator(pipeline, options));
  }

  public async aggregateIterator(
    pipeline: any,
    options?: any,
  ): Promise<CursorIterator<any>> {
    options = options || {};
    if (options.allowDiskUse == null) {
      options.allowDiskUse = true;
    }
    const cursor = await this.db
      .collection(this.collection)
      .aggregate(pipeline, options);
    // @ts-ignore
    return new CursorIterator(cursor);
  }

  public async distinct(field: string) {
    return await this.db.collection(this.collection).distinct(field, {});
  }

  public async update(query: any, updates: any): Promise<NongoWriteResult> {
    query = this.prepareQuery(query);
    const res = await this.db
      .collection(this.collection)
      .updateMany(query, updates);
    return res.result;
  }

  public async count(query = {}): Promise<number> {
    return await this.db.collection(this.collection).countDocuments(query);
  }

  /*
   * Return the schema for this model. See getSchema for an understanding of inheritance.
   */
  public abstract defineSchema(): any;

  public defineSchemaNoIndex(): any {
    const schema = this.defineSchema();
    this.removeSchemaIndex(schema);
    return schema;
  }

  /*
   * Must be run on all queries before they are used.
   */
  public prepareQuery(query) {
    query = clone(query);
    const {_id} = query;
    if (!_id) {
      return query;
    }

    // Make sure that _id values are ObjectIds not strings
    if (typeof _id === 'string') {
      query._id = Nongo.toObjectId(query._id);
    } else if (_id.$in) {
      _id.$in = _id.$in.map((el) => typeof el === 'string' ? Nongo.toObjectId(el) : el);
    }

    return query;
  }

  /*
   * This method should be overridden if want to use something other than the
   * class name as the collection name.
   */
  protected defineCollection() {
    return this.constructor.name;
  }

  private throwValidationError(errors: string[]) {
    let objStr;
    try {
      objStr = `failed to save the following obj: ${JSON.stringify(this.obj)}`;
    } catch (err) {
      objStr = `failed to stringify obj: ${err.message}`;
    }
    throw Error(`${JSON.stringify(errors)} ${objStr}`);
  }

  /**
   * Recursively remove all the keys form the schema that are used to create indexes.
   *
   * @param parentSchema
   */
  private removeSchemaIndex(parentSchema: any) {
    for (const key of Object.keys(parentSchema)) {
      // Delete any of the keys that can be used to create indexes
      const schema = parentSchema[key];
      delete schema.unique;
      delete schema.indexed;
      delete schema.text;

      // Make a recursive call if required
      const type = schema.type;
      if (Array.isArray(type)) {
        const elType = type[0];
        if (typeof elType === 'object') {
          this.removeSchemaIndex(elType);
        }
      } else if (typeof type === 'object') {
        this.removeSchemaIndex(type);
      }
    }
  }

  private async toArray(cursorIterator: CursorIterator<this>): Promise<this[]> {
    const arr = [];
    // @ts-ignore
    for await (const m of cursorIterator) {
      arr.push(m);
    }
    return arr;
  }

  /*
   * Initialises any objects and arrays which are marked as required in the schema.
   */
  private initStructure() {
    // Init this.obj if it is null
    if (this.obj == null) {
      this.obj = {} as T;
    }

    // We get the schema from nongo not this.getSchema() for the sake of efficiency
    if (this.nongo && this.nongo.schema[this.name]) {
      this.initKey(this.obj, this.nongo.schema[this.name]);
    }
  }

  private initKey(parent, parentSchema) {
    for (const key of Object.keys(parentSchema)) {
      const schema = parentSchema[key];
      const type = schema.type;

      // Evaluate schema.required to a boolean
      let required = Array.isArray(schema.required)
        ? schema.required[0]
        : schema.required;
      required =
        typeof required === 'function'
          ? required(parent[key], this.obj)
          : required;

      const isNullRequired = parent[key] == null && required;

      if (Array.isArray(type)) {
        if (isNullRequired) {
          parent[key] = [];
        }

        // Make recursive call if not null and an array and child type is not native type
        if (parent[key] != null && Array.isArray(parent[key])) {
          const childType = type[0];
          if (typeof childType === 'object') {
            parent[key].forEach((el) => this.initKey(el, childType));
          }
        }
      } else if (typeof type === 'object') {
        // Init the the object if null and required
        if (isNullRequired) {
          parent[key] = {};
        }

        // Make recusive call if not null and an object
        if (parent[key] != null && typeof parent[key] === 'object') {
          this.initKey(parent[key], type);
        }
      } else if (type === 'object' && isNullRequired) {
        parent[key] = {};
      }
    }
  }
}
