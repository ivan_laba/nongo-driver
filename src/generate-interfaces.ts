import fs from 'fs';
import glob from 'glob';
import minimist from 'minimist';
import path from 'path';
import Logger from 'wbb-logger';
import Model from './model';
import TsInterfaceGenerator from './ts-interface-generator';

const logger = new Logger('generate-interfaces.ts');

const getInterfacePath = (modelPath: string) =>
  modelPath.replace(/.nongo\.ts$/, '-obj.ts');

const createInterfaces = async (modelDir: string, placeholder: boolean) => {
  const modelPaths = glob
    .sync(path.join(modelDir, '/**/*.nongo.ts'))
    .map((p) => path.resolve(p));

  // Generate placeholder interfaces
  if (placeholder) {
    for (const modelPath of modelPaths) {
      fs.writeFileSync(
        getInterfacePath(modelPath),
        'export default interface TempObj {\n  [key: string]: any;\n}\n',
      );
    }
    return;
  }

  while (modelPaths.length) {
    const modelPath = modelPaths.shift();

    // Find out information about the models super class
    const modelCode = fs.readFileSync(modelPath, 'utf8');
    const classDef = modelCode.match(/ *export +default.+class.*/).shift();
    const superClass = classDef
      .split('extends')
      .pop()
      .replace(/\{/g, '')
      .split('<')
      .shift()
      .trim();
    const superClassImport = modelCode
      .match(new RegExp('.*import .*' + superClass + '[ ,}].*'))
      .shift();
    const superClassPath = path.resolve(
      path.dirname(modelPath),
      superClassImport
        .match(/'.*'|".*"/)
        .shift()
        .replace(/['"]/g, '') + '.ts',
    );

    // If we haven't already processed the super class do it before this model
    const indexOfSP = modelPaths.indexOf(superClassPath);
    if (indexOfSP >= 0) {
      modelPaths.splice(indexOfSP, 1);
      modelPaths.unshift(modelPath);
      modelPaths.unshift(superClassPath);
      continue;
    }

    // Create the temporary interface if this model has a super class that will enable us to import the class
    const interfaceFile = getInterfacePath(modelPath);
    if (superClass !== Model.name) {
      const tempObj = fs
        .readFileSync(getInterfacePath(superClassPath), 'utf8')
        .replace(superClass + 'Obj', 'TempObj')
        .replace('{', '{\n  [key: string]: any;');
      fs.writeFileSync(interfaceFile, tempObj);
    }

    // Generate the actual interface for the model
    const model = (await import(modelPath)).default;
    new TsInterfaceGenerator().generateInterface(model, interfaceFile);
  }
};

const generateInterfaces = async (modelDir: string) => {
  logger.info(`Generating interfaces for ${modelDir}...`);
  await createInterfaces(modelDir, true);
  await createInterfaces(modelDir, false);
  logger.info(`Finished generating interfaces for ${modelDir}`);
};

export default generateInterfaces;

if (require.main === module) {
  // Read the args
  const args = minimist(process.argv.slice(2));
  const {modelDir} = args;
  if (!modelDir) {
    throw Error('--modelDir <file path> is required');
  }
  generateInterfaces(modelDir).catch((err) => logger.error(err));
}
