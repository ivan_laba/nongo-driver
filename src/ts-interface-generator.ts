import * as fs from 'fs';
import {Newable} from './interfaces';
import Model from './model';
import NongoMap from './nongo-map';

const nongo2TS = {
  string: 'string',
  number: 'number',
  boolean: 'boolean',
  date: 'Date',
  object: 'any',
  any: 'any',
  objectId: 'any',
};

/**
 * Used to generate typescript interfaces from nongo schema and write them to a file.
 *
 * @author Stuart
 */
export default class TsInterfaceGenerator {

  /**
   * @param {Newable} ModelClass - the constructor for the model you want to generate an interface for
   * @param file
   * @returns {string}
   */
  public generateInterface(ModelClass: Newable, file) {
    const schema = new ModelClass().defineSchema();
    schema._id = {
      type: 'objectId',
    };
    const code =
      `// This file was generated using nongo-driver's TsInterfaceGenerator.\n` +
      `export default interface ${ModelClass.name}Obj ${this.toInterface(schema)}\n`;
    fs.writeFileSync(file, code);
  }

  /**
   * Convert the schema into an interface
   *
   * @param schema - the nongo schema
   * @param {number} depth - recursion depth used to indent the code generated
   * @returns {string}
   */
  private toInterface(schema: any, depth = 0) {
    const indent = '  ';
    let code = '{\n';

    for (const key of Object.keys(schema)) {
      // Get the type from the schema
      const subSchema = schema[key];
      let nongoType = subSchema.type;

      // if nongoType is a subclass of Model, instantiate model and get defined schema
      if (typeof nongoType === 'function' && nongoType.prototype instanceof Model) {
        nongoType = new nongoType.prototype.constructor(null, null).defineSchema();
      }

      // i.e. it's set and it's not a function
      const schemaValueIsAlwaysTrue = (schemaValue) =>  {
        schemaValue = Array.isArray(schemaValue) ? schemaValue[0] : schemaValue;
        return typeof schemaValue === 'boolean' ? schemaValue : false;
      };

      // Decide if the key is required
      const required = schemaValueIsAlwaysTrue(subSchema.required) || schemaValueIsAlwaysTrue(subSchema.notEmpty);

      // Add the interface key
      code += indent.repeat(depth + 1) + `'${key}'${required ? '' : '?'}: `;

      // If an object or an array do some recursion
      if (typeof nongoType === 'object') {

        const newDepth = depth + 1;
        if (Array.isArray(nongoType)) {
          const el = nongoType[0];
          if (typeof el === 'object') {
            code += `Array<${this.toInterface(el, newDepth)}>`;
          } else {
            code += el + '[]';
          }
        } else if (nongoType instanceof NongoMap) {
          code += `{[key: string]: ${this.toInterface(nongoType.valueType, newDepth)}}`;
        } else {
          code += this.toInterface(nongoType, newDepth);
        }

        // else just set the type
      } else {
        const tsType = nongo2TS[nongoType];
        if (!tsType) {
          throw new Error(`${nongoType} cannot be converted to a typescript type`);
        }
        code += tsType;
      }
      code += ';\n';

    }

    code += indent.repeat(depth) + '}';

    return code;

  }

}
