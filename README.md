
## How to create a release

### 1. Commit all changes
Run `$ git status` and make sure you have no uncommitted changes

### 2. Run Build
This command builds the source into the folder `dist` 
```
$ gulp
```

### 3. Stage changes
```
$ git add -u
```

### 4. Commit Changes
```
$ git commit -m "dist"
```

### 5. Update version number
update version number using semantic versioning release types (patch, major, or minor)
e.g. 

```
$ npm version patch
```

### 6. Login to npm
```
$ npm login
```

### 7. Publish npm
```
$ npm publish
```

### 8. Create a new branch `dist`
```
$ git checkout -b dist
```

### 9. Push dist to origin
```
$ git push origin dist
```

### 10. Create a pull request
Create a pull request to merge `origin/dist` --> `upstream/master`
Use version number as pull request title.

### 11. Push tags to upstream
```
$ git push upstream --tags
```
