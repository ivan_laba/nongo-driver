import {MongoClient} from 'mongodb';
import {MongoMemoryServer} from 'mongodb-memory-server';
import {Newable, NongoParams} from '../src';
import Nongo from '../src/nongo';

const mongoMemoryServer = new MongoMemoryServer();
export default class DatabaseHelper {
  public static async getNongoInstance(models: Newable[]): Promise<Nongo> {
    return await new Nongo(
      await DatabaseHelper.getNongoParams(),
      models,
    ).connect();
  }

  public static async getNongoParams(): Promise<NongoParams> {
    return {
      host: 'localhost',
      port: await mongoMemoryServer.getPort(),
      db: await mongoMemoryServer.getDbName(),
    };
  }

  public static async getClient(): Promise<MongoClient> {
    return MongoClient.connect(await mongoMemoryServer.getUri(), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  }

  public static async getDbName(): Promise<string> {
    return await mongoMemoryServer.getDbName();
  }

  public static async drop(): Promise<void> {
    const client = await DatabaseHelper.getClient();
    const db = client.db(await DatabaseHelper.getDbName());
    await db.dropDatabase();
    await client.close();
  }
}
