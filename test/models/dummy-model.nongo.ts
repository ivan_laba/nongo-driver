import Model from '../../src/model';
import MongoIndex from '../../src/mongo-index';
import Nongo from '../../src/nongo';
import DummyModelObj from './dummy-model-obj';
import NongoMap from '../../src/nongo-map';

export default class DummyModel extends Model<DummyModelObj> {

  constructor(nogo: Nongo, obj: any) {
    super(nogo, obj);
  }

  public getMongoIndexes(): MongoIndex[] {
    return [
      new MongoIndex({dontStripChildren: 1, created: 1}),
      new MongoIndex({age: 'text'}),
    ];
  }

  public defineSchema(): any {
    return {

      dontStripChildren: {
        type: 'object',
      },

      dontStripChildren2: {
        type: 'any',
      },

      exampleMap: {
        type: new NongoMap({

          mapValueField: {
            type: 'string',
            required: true,
          },

        }),
      },

      created: {
        type: 'date',
      },

      arrayOfObject: {
        type: ['object'],
      },

      name: {
        type: 'string',
        unique: true,
        required: true,
        validate: [
          (value, obj) => value !== 'invalid name',
          (value, obj) => '"' + value + '" is not a valid name',
        ],
      },

      age: {
        type: 'number',
        required: [true, 'DummyModels must have an age'],
      },

      pets: {
        required: [true, 'pets array must be present'],
        validate: [
          (value, obj) => value.length === 2,
          (value, obj) => '***path*** should only have 2 elements',
        ],
        type: [
          {

            species: {
              type: 'string',
            },

            likes: {

              type: {

                food: {
                  type: ['string'],
                },

                drink: {
                  indexed: true,
                  type: ['string'],
                },

              },

            },

          },
        ],
      },

      job: {
        required: [true, 'The job field is required'],
        type: {

          role: {
            type: 'string',
            required: [true, 'job.role must be set'],
            indexed: true,
          },

          at: {
            type: 'string',
            required: [
              (val, obj) => obj.job.role,
              'job.at must be set if job.role is',
            ],
          },

        },

      },

      location: {
        type: {

          address1: {
            type: 'string',
          },

        },
      },

      autoInit: {
        required: true,
        type: {

          initArray: {
            required: true,

            type: [{
              nestedObj: {
                required: true,
                type: 'object',
              },
            }],

          },

          initNestedObj: {
            required: (val, obj) => true,
            type: {
              hello: {
                type: 'string',
              },
            },
          },

          initNestedNative: {
            required: [
              (val, obj) => true,
              '***path*** is required',
            ],
            type: 'object',
          },

        },
      },

      notEmptyFields: {
        type: {

          aString: {
            type: 'string',
            notEmpty: [true, '***path*** must be a string with at least one character'],
          },

          anArray: {
            type: ['string'],
            notEmpty: true,
          },

        },
      },

    };
  }

}
